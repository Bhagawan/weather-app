package com.example.weatherapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView data, week_day, temp;
        public ImageView condit;

        public ViewHolder(View itemView) {
            super(itemView);

            data = (TextView) itemView.findViewById(R.id.day_data);
            week_day = (TextView) itemView.findViewById(R.id.day_of_week);
            temp = (TextView) itemView.findViewById(R.id.temperature);
            //cond = (TextView) itemView.findViewById(R.id.conditions);
            condit = (ImageView) itemView.findViewById(R.id.weathercondition);
        }
    }

//    private Weather mWeather;
//    private ArrayList<Bitmap> img;
    private WeatherandImages wAndi;

    public ContactsAdapter(WeatherandImages weather) {
        wAndi = weather;

//        mWeather = weather.weather;
//        img = weather.img;
//        img.clear();
//        for (int i=0; i< weather.img.size(); i++) {
//            img.add(weather.img.get(i));
//        }

    }

    public static String getDayStringOld(Date date, Locale locale) {
        DateFormat formatter = new SimpleDateFormat("EEE", locale);
        return formatter.format(date);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.row_layout, parent, false);

        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        TextView textView1 = holder.data;

        Date date;
        Locale locale = Locale.getDefault();
        SimpleDateFormat myFormat = new SimpleDateFormat("MMMM d");
        String dayofWeek = "Error";
        String date2 ="Error";
        try {
            date = new SimpleDateFormat("yyyy-MM-dd",locale).parse(wAndi.weather.forecast.forecastday[position].date);
            dayofWeek = getDayStringOld(date,locale);
            date2 = myFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        textView1.setText(date2);
        TextView textView2 = holder.week_day;
        textView2.setText(dayofWeek);
        TextView textView3 = holder.temp;
        textView3.setText(wAndi.weather.forecast.forecastday[position].day.avgtemp_c);
        ImageView imageView = holder.condit;
        imageView.setImageBitmap(wAndi.img.get(position));
    }

    @Override
    public int getItemCount() {
        return wAndi.weather.forecast.forecastday.length;
    }

}

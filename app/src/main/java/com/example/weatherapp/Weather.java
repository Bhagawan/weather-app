package com.example.weatherapp;

public class Weather {
    Location  location;
    Current current;
    Forecast forecast;

    class Forecast {
        Forecastday[] forecastday;

        class Forecastday {
            String date;
            Day day;

            class Day {
                String avgtemp_c;
                Condition condition;

                class Condition {
                    String text;
                    String icon;
                }
            }
        }

    }

    class Location {
        String name;
        String country;
    }

    class Current {
        String temp_c;
    }

    public void equate(Weather weather) {
        for(int i = 0; i < this.forecast.forecastday.length; i++) {
            this.forecast.forecastday[i].date = weather.forecast.forecastday[i].date;
            this.forecast.forecastday[i].day = weather.forecast.forecastday[i].day;
            this.forecast.forecastday[i].day.condition.text = weather.forecast.forecastday[i].day.condition.text;
            this.forecast.forecastday[i].day.condition.icon = weather.forecast.forecastday[i].day.condition.icon;
        }

    }

}

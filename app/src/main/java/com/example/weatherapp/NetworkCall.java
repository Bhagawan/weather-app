package com.example.weatherapp;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.Image;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkCall extends AsyncTask<String, Void, WeatherandImages> {
    public AsyncResponse delegate = null;

    @Override
    protected WeatherandImages doInBackground(String... strings) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://api.weatherapi.com/").
                addConverterFactory(GsonConverterFactory.create()).build();
        WeatherService service = retrofit.create(WeatherService.class);
        Call<Weather> call = service.getWeather(strings[0],7);

        try {
            Response<Weather> response = call.execute();
            Weather weather = response.body();
            ArrayList<Bitmap> img = new ArrayList<>();

            for(int i = 0; i<weather.forecast.forecastday.length; i++) {
                java.net.URL url = new java.net.URL("http:" + weather.forecast.forecastday[i].day.condition.icon);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                img.add(getResizedBitmap(myBitmap,100,100));
            }

            return new WeatherandImages(weather,img);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(WeatherandImages result) {
        if( result != null ) delegate.processFinish(result);
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
    }

}

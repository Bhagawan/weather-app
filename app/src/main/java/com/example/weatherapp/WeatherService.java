package com.example.weatherapp;

import retrofit2.*;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {
    @GET("v1/forecast.json?key=ee94006c9bb74ce892f181126211305")
    Call<Weather> getWeather(@Query("q") String city, @Query("days") int week);
}
//https://api.weatherapi.com/v1/forecast.json?key=ee94006c9bb74ce892f181126211305&q=auto:ip
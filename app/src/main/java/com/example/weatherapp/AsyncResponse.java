package com.example.weatherapp;

public interface AsyncResponse {
    void processFinish(WeatherandImages output);
}

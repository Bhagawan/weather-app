package com.example.weatherapp;



import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements AsyncResponse{
    private View mainview;
    private ConstraintLayout mainScreen;
    private NetworkCall weatherCheck;
    RecyclerView outputView;
    Weather oweather = new Weather();
    WeatherandImages weatherandImages;
    ContactsAdapter adapter;
    private TextView cityName,currenttemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mainview = getWindow().getDecorView();
        mainview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        EditText cityInput = findViewById(R.id.cityInput);
        Activity currAct = this;

        cityInput.setOnEditorActionListener((v, actionId, event) -> {
            switch (actionId){
                case EditorInfo.IME_ACTION_DONE:
                    getWeather(cityInput.getText().toString());
                    hideSoftKeyboard(currAct);
                    break;
                case EditorInfo.IME_ACTION_NEXT:
                case EditorInfo.IME_ACTION_PREVIOUS:
                    return true;
            }
            return false;
        });

        outputView = findViewById(R.id.ouputview);
        cityName = findViewById(R.id.cityname);
        currenttemp = findViewById(R.id.currentcitytemp);
        mainScreen = findViewById(R.id.mainLayout);
    }

    private void getWeather(String city) {
        weatherCheck = new NetworkCall();
        weatherCheck.delegate = this;
        weatherCheck.execute(city);
    }

    @Override
    public void processFinish(WeatherandImages weather) {
        if (weather != null) {
            setWeather(weather);
        }
        else setError();
    }

    private void setError() {
    }

    @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
    private void setWeather(WeatherandImages weather) {

        cityName.setText(weather.weather.location.name);
        currenttemp.setText((weather.weather.current.temp_c) + "°");
        if (adapter == null) {
            weatherandImages = weather;
            adapter = new ContactsAdapter(weatherandImages);
            outputView.setAdapter(adapter);
            outputView.setLayoutManager(new LinearLayoutManager(this));
        }
        else {
            weatherandImages.setData(weather);
            adapter.notifyDataSetChanged();
        }

    }

    public void hideSoftKeyboard(Activity activity) {
        fullscreen(activity.findViewById(R.id.mainLayout));
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.findViewById(R.id.mainLayout).getWindowToken(), 0);
        activity.findViewById(R.id.mainLayout).requestFocus();
    }

    private void fullscreen(View view) {
        view.findViewById(R.id.mainLayout).setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}
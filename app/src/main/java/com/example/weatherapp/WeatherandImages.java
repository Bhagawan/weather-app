package com.example.weatherapp;

import android.graphics.Bitmap;

import java.util.ArrayList;

public class WeatherandImages {
    Weather weather;
    ArrayList<Bitmap> img;

    public WeatherandImages(Weather weather, ArrayList<Bitmap> img) {
        this.weather = weather;
        this.img = img;
    }

    public void setData(Weather weather, ArrayList<Bitmap> img) {
        this.weather.equate(weather);
        this.img = new ArrayList<>();
        this.img.addAll(img);
    }

    public void setData(WeatherandImages weatherandImages) {
        this.weather.equate(weatherandImages.weather);
        this.img.clear();
        this.img.addAll(weatherandImages.img);
    }
}
